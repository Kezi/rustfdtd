pub(crate) fn to_color(r: u8, g: u8, b: u8) -> u32 {
    let mut ret = 0xff000000_u32;
    ret |= b as u32;
    ret |= (g as u32) << 8;
    ret |= (r as u32) << 16;
    ret
}

fn to_colorf(r: f64, g: f64, b: f64) -> u32 {
    let r = (r * 255.0) as u8;
    let g = (g * 255.0) as u8;
    let b = (b * 255.0) as u8;

    //println!("{}", r);
    let mut ret = 0xff000000_u32;
    ret |= b as u32;
    ret |= (g as u32) << 8;
    ret |= (r as u32) << 16;
    ret
}

fn from_colorf(color: u32) -> (f64, f64, f64) {
    (
        (((color >> 16) & 0xff) as f64) / 255.0,
        (((color >> 8) & 0xff) as f64) / 255.0,
        (((color) & 0xff) as f64) / 255.0,
    )
}

pub(crate) fn jet(v: f64) -> u32 {
    let mut r = 1.0;
    let mut g = 1.0;
    let mut b = 1.0;

    let vmax = 1.0;
    let vmin = -1.0;

    let mut v = v;

    if v < vmin {
        v = vmin;
    }
    if v > vmax {
        v = vmax;
    }

    let dv = vmax - vmin;

    if v < (vmin + 0.25 * dv) {
        r = 0.0;
        g = 4.0 * (v - vmin) / dv;
    } else if v < (vmin + 0.5 * dv) {
        r = 0.0;
        b = 1.0 + 4.0 * (vmin + 0.25 * dv - v) / dv;
    } else if v < (vmin + 0.75 * dv) {
        r = 4.0 * (v - vmin - 0.5 * dv) / dv;
        b = 0.0;
    } else {
        g = 1.0 + 4.0 * (vmin + 0.75 * dv - v) / dv;
        b = 0.0;
    }

    to_colorf(r, g, b)
}

pub(crate) fn blend_colors(fg: u32, bg: u32, fg_a: f64) -> u32 {
    let bg_a = 1.0;
    let r_r;
    let r_g;
    let r_b;

    let fg = from_colorf(fg);
    let bg = from_colorf(bg);

    let r_a = 1. - (1. - fg_a) * (1. - bg_a); // 0.75
    r_r = fg.0 * fg_a / r_a + bg.0 * bg_a * (1. - fg_a) / r_a; // 0.67
    r_g = fg.1 * fg_a / r_a + bg.1 * bg_a * (1. - fg_a) / r_a; // 0.33
    r_b = fg.2 * fg_a / r_a + bg.2 * bg_a * (1. - fg_a) / r_a; // 0.00

    to_colorf(r_r, r_g, r_b)
}
