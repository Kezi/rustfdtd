use ndarray::Zip;

use crate::lib::EmField;
use crate::lib::FdtdEngine;

pub struct CpuFdtd {
    pub field: EmField,
}

impl FdtdEngine for CpuFdtd {
    fn new(w: usize, h: usize) -> Self {
        CpuFdtd {
            field: EmField::new(w, h),
        }
    }

    fn precompute_material(&mut self) {
        for i in 0..self.field.H.nrows() {
            for j in 0..self.field.H.ncols() {
                let mut temp = (self.field.mat[(i, j)].sigma * self.field.time_step)
                    / (self.field.mat[(i, j)].epsilon * 2.0);
                self.field.K[(i, j, 0)] = (1.0 - temp) / (1.0 + temp); // Ca
                self.field.K[(i, j, 1)] = (self.field.time_step
                    / (self.field.mat[(i, j)].epsilon * self.field.grid_cell_size))
                    / (1.0 + temp); // Cb
                temp = (self.field.mat[(i, j)].sigma * self.field.time_step)
                    / (self.field.mat[(i, j)].mu * 2.0);
                self.field.K[(i, j, 2)] = (1.0 - temp) / (1.0 + temp); // Da
                self.field.K[(i, j, 3)] = (self.field.time_step
                    / (self.field.mat[(i, j)].mu * self.field.grid_cell_size))
                    / (1.0 + temp);
                // Db
            }
        }
    }

    fn step(&mut self) {
        // h step

        let width = self.field.H.nrows();
        let height = self.field.H.ncols();

        Zip::indexed(&mut self.field.H).par_for_each(|(i, j), x| {
            if i > width - 2 || j > height - 2 {
                return;
            }
            *x = self.field.K[(i, j, 2)] * *x
                + self.field.K[(i, j, 3)]
                    * (self.field.E[(i, j + 1, 0)] - self.field.E[(i, j, 0)]
                        + self.field.E[(i, j, 1)]
                        - self.field.E[(i + 1, j, 1)])
        });

        Zip::indexed(&mut self.field.E).par_for_each(|(i, j, k), x| {
            if i < 1 || j < 1 {
                return;
            }
            if k == 0 {
                *x = self.field.K[(i, j, 0)] * *x
                    + self.field.K[(i, j, 1)] * (self.field.H[(i, j)] - self.field.H[(i, j - 1)]);
            } else if k == 1 {
                *x = self.field.K[(i, j, 0)] * *x
                    + self.field.K[(i, j, 1)] * (self.field.H[(i - 1, j)] - self.field.H[(i, j)])
            }
        });
    }

    fn render(&self, buffer: &mut ndarray::Array2<u32>) {
        Zip::indexed(buffer).par_for_each(|(i, j), x| *x = crate::utils::jet(self.field.H[(i, j)]));
    }
}
