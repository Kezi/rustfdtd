extern crate minifb;
use minifb::{Key, Window, WindowOptions};

use ndarray::prelude::*;
use ndarray::Array;

use crate::lib::FdtdEngine;

mod cpu_parallel;
mod image;
mod lib;
mod utils;

fn t() -> f64 {
    std::time::SystemTime::now()
        .duration_since(std::time::SystemTime::UNIX_EPOCH)
        .unwrap()
        .as_secs_f64()
}

fn main() {
    let args: Vec<String> = std::env::args().collect();

    let image = image::read_image(&args[1]);

    let mut fdtd = cpu_parallel::CpuFdtd::new(image.shape()[0], image.shape()[1]);

    assert!(fdtd.field.width == image.shape()[0]);

    let mut buffer: Array2<u32> = Array::<u32, _>::zeros((fdtd.field.width, fdtd.field.height).f());

    let mut window = Window::new(
        "fdtd",
        fdtd.field.width,
        fdtd.field.height,
        WindowOptions::default(),
    )
    .unwrap_or_else(|e| {
        panic!("{}", e);
    });

    //window.limit_update_rate(Some(std::time::Duration::from_micros(16600)));

    let scene = image::decode_image(image.clone());

    fdtd.field.mat = scene.mat.clone();

    fdtd.precompute_material();

    let mut k = 0;

    let mut oldsec: f64 = 0.0;
    let mut oldframe = 0;

    while window.is_open() && !window.is_key_down(Key::Escape) {
        k += 1;
        let t = t();

        for i in &scene.sine_sources {
            fdtd.field.H[(i.0, i.1)] = f64::sin(k as f64 / 5.0) * 30.0;
        }

        for i in &scene.gaussian_sources {
            fdtd.field.H[(i.0, i.1)] = f64::powf(
                std::f64::consts::E,
                -(f64::powf(k as f64 - 60.0, 2.0) / 150.0),
            ) * 100.0;
        }

        fdtd.step();

        if oldsec.floor() < t.floor() {
            println!("{} fps, t={}s", k - oldframe, t);
            oldframe = k;
            oldsec = t;
        }

        fdtd.render(&mut buffer);

        buffer.indexed_iter_mut().for_each(|(coord, f)| {
            let img = image[coord];
            match img {
                0xffffff => *f = 0,
                0 => {}
                _ => *f = utils::blend_colors(img, *f, 0.4),
            }
        });

        window
            .update_with_buffer(
                buffer.as_slice_memory_order().unwrap(),
                fdtd.field.width,
                fdtd.field.height,
            )
            .unwrap();
    }
}
