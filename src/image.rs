use std::collections::HashSet;

use crate::lib::Material;
use image::Pixel;
use ndarray::prelude::*;
use ndarray::Array;

use image::GenericImageView;

pub(crate) fn read_image(file: &str) -> Array2<u32> {
    let img = image::open(file).unwrap();

    println!("loaded image, dimensions {:?}", img.dimensions());

    let mut buffer: Array2<u32> =
        Array::<u32, _>::zeros((img.width() as usize, img.height() as usize).f());

    for i in 0..buffer.nrows() as usize {
        //per ogni riga
        for j in 0..buffer.ncols() as usize {
            //per ogni colonna
            let px = img.get_pixel(i.try_into().unwrap(), j.try_into().unwrap());
            let chan = px.channels();
            buffer[(i, j)] = chan[2] as u32 + ((chan[1] as u32) << 8) + ((chan[0] as u32) << 16);
        }
    }

    buffer
}

#[derive(Default, Debug)]
pub struct Scene {
    pub sine_sources: HashSet<(usize, usize)>,
    pub gaussian_sources: HashSet<(usize, usize)>,

    pub mat: Array2<Material>,
}

pub fn decode_image(image: Array2<u32>) -> Scene {
    let mut scene = Scene {
        mat: Array::default((image.shape()[0], image.shape()[1]).f()),
        ..Default::default()
    };

    image.indexed_iter().for_each(|(coords, d)| {
        match d {
            0xff0000 => {
                scene.sine_sources.insert(coords);
            }
            0xc0c0c0 => {
                scene.gaussian_sources.insert(coords);
            }

            0xffffff => {
                scene.mat[coords] = Material {
                    mu: 10.0,
                    epsilon: 8.854187817620e-12,
                    sigma: 0.0,
                }
            } //conductor
            0x0000FF => {
                scene.mat[coords] = Material {
                    mu: 1.25663706212e-6,
                    epsilon: 2e-11,
                    sigma: 0.0,
                }
            } // lens
            0x00FFFF => {
                scene.mat[coords] = Material {
                    mu: 1.25663706212e-6,
                    epsilon: 45.854187817620e-12,
                    sigma: 0.0,
                }
            } //waveguide
            0x800000 => {
                scene.mat[coords] = Material {
                    mu: 1.25663706212e-6,
                    epsilon: 8.854187817620e-12,
                    sigma: 0.2,
                }
            } //absorber

            0x000000 => {
                scene.mat[coords] = Material {
                    mu: 1.25663706212e-6,
                    epsilon: 8.854187817620e-12,
                    sigma: 0.00,
                }
            }
            _ => {}
        }
    });

    scene
}
