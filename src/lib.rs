use ndarray::prelude::*;
use ndarray::Array;

#[derive(Clone, Debug)]
pub struct Material {
    pub mu: f64,
    pub epsilon: f64,
    pub sigma: f64,
}

impl Default for Material {
    fn default() -> Self {
        Self {
            mu: 1.25663706212e-6, // vacuum permeability (μ)
            epsilon: 8.854187817620e-12,
            sigma: 0.00,
        }
    }
}

#[derive(Clone)]
pub struct EmField {
    pub width: usize,
    pub height: usize,
    pub(crate) H: Array2<f64>,
    pub(crate) E: Array3<f64>,
    pub(crate) mat: Array2<Material>,
    pub(crate) K: Array3<f64>,   // precomputed constant values Ca Cb Da Db
    pub(crate) out: Array2<f64>, // pixel array
    pub time_step: f64,
    pub grid_cell_size: f64,
}

impl EmField {
    pub fn new(w: usize, h: usize) -> Self {
        EmField {
            H: Array::<_, _>::zeros((w, h).f()),
            E: Array::<_, _>::zeros((w, h, 2).f()),
            mat: Array::<_, _>::default((w, h).f()),
            K: Array::<_, _>::zeros((w, h, 4).f()),
            out: Array::<_, _>::zeros((w, h).f()),
            time_step: 1e-12,
            grid_cell_size: 1e-3,
            width: w,
            height: h,
        }
    }
}

pub trait FdtdEngine {
    fn new(w: usize, h: usize) -> Self;
    fn precompute_material(&mut self);
    fn step(&mut self);
    fn render(&self, buffer: &mut Array2<u32>);
}
